The task is to create a REST API for a simple to do list application. 
The app must have:

1. Add a task
2. Update a task
3. Delete a task
4. Get a paginated list of tasks

A task can have a :
1. Name
2. Task completion date
3. Task creation date
4. Description

The developer is free to choose the language of implementation. The choices are:

1. Java/Groovy with Spring Boot
2. Javascript with Node.js
3. Scala with Play framework

We are looking for:

1. Code documentation
2. Unit test cases
3. Loose coupling and strong cohesion 
4. Should follow DRY principles 


Create a clone of this repository and submit a pull request.

PS. The data must be exchanged via JSON. No XML. Also, the developer can choose between a relational DB like MySQL or Postgres or Mongodb.